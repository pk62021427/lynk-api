package seed

import (
	"encoding/json"
	"fmt"

	"github.com/bxcodec/faker/v3"
)

func RandomInt(min int, max int) int {
	rnd, _ := faker.RandomInt(min, max)
	return rnd[0]
}

func RandomBool() bool {
	rnd := RandomInt(0, 1)
	if rnd == 1 {
		return true
	} else {
		return false
	}
}
func RandomImage() string {
	return fmt.Sprintf("https://via.placeholder.com/150?text=%s", faker.Word())
}

type Any map[string]interface{}

func Seed(length int, cb func() Any) []Any {
	var data []Any

	table := MakeSlice(length, func() interface{} {
		return cb()
	})

	b, _ := json.Marshal(table)

	json.Unmarshal(b, &data)

	return data
}

func MakeSlice(length int, cb func() interface{}) []interface{} {
	var data []interface{}

	for i := 0; i < length; i++ {
		data = append(data, cb())
	}

	return data
}
