package seed

import (
	"github.com/bxcodec/faker/v3"
)

func SeedUser(length int) []Any {

	table := Seed(length, func() Any {
		return Any{
			"email":           faker.Email(),
			"password":        "123456",
			"confirmPassword": "123456",
			"firstName":       faker.FirstName(),
			"lastName":        faker.LastName(),
			"username":        faker.Username(),
			"avatar":          RandomImage(),
		}
	})

	return table
}
