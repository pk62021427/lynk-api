package seed

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
)

type Configs struct {
	Headers map[string]string
	BaseUrl string
}

type Request interface {
	Get(uri string) (*http.Response, error)
	Post(uri string, body interface{}) (*http.Response, error)
	Patch(uri string, body io.Reader) (*http.Response, error)
	Delete(uri string) (*http.Response, error)
}

func NewRequest(configs Configs) Request {
	return &configs
}

func (c *Configs) Get(uri string) (*http.Response, error) {
	req, err := http.NewRequest("GET", c.BaseUrl+uri, nil)
	if err != nil {
		return nil, err
	}

	client := &http.Client{}
	res, err := client.Do(req)

	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	return res, nil
}

func (c *Configs) Post(uri string, body interface{}) (*http.Response, error) {
	j, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}
	b := bytes.NewBuffer(j)
	req, err := http.NewRequest("POST", c.BaseUrl+uri, b)

	for k, v := range c.Headers {
		req.Header.Set(k, v)
	}

	if err != nil {
		return nil, err
	}
	client := &http.Client{}
	res, err := client.Do(req)

	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	return res, nil

}

func (c *Configs) Patch(uri string, body io.Reader) (*http.Response, error) {
	return nil, nil
}

func (c *Configs) Delete(uri string) (*http.Response, error) {
	return nil, nil
}
