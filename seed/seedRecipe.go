package seed

import (
	"github.com/bxcodec/faker/v3"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func SeedRecipe(length int, userId primitive.ObjectID) []Any {

	table := Seed(length, func() Any {
		return Any{
			"title":    faker.Word(),
			"image":    RandomImage(),
			"userId":   userId,
			"amount":   RandomInt(1, 10),
			"duration": RandomInt(1, 10) * 60,
			"public":   RandomBool(),
			"ingredients": MakeSlice(RandomInt(1, 10), func() interface{} {
				return Any{
					"name":   faker.Word(),
					"image":  RandomImage(),
					"amount": RandomInt(1, 15),
					"unit":   faker.Word(),
				}
			}),
			"steps": MakeSlice(RandomInt(1, 10), func() interface{} {
				return Any{

					"text": faker.Paragraph(),
					"images": MakeSlice(RandomInt(1, 3), func() interface{} {
						return RandomImage()
					}),
				}
			}),
		}
	})

	return table
}
