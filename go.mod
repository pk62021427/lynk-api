module lynk

// +heroku goVersion go1.16
go 1.16

require (
	github.com/aws/aws-sdk-go v1.34.28 // indirect
	github.com/bxcodec/faker/v3 v3.6.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/go-stack/stack v1.8.1 // indirect
	github.com/gofiber/fiber v1.14.6
	github.com/gofiber/fiber/v2 v2.14.0 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/kr/fs v0.1.0 // indirect
	github.com/kr/pretty v0.3.0 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/rogpeppe/go-internal v1.8.0 // indirect
	github.com/tools/godep v0.0.0-20180126220526-ce0bfadeb516 // indirect
	github.com/youmark/pkcs8 v0.0.0-20201027041543-1326539a0a0a // indirect
	go.mongodb.org/mongo-driver v1.7.3 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/sys v0.0.0-20211007075335-d3039528d8ac // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.7 // indirect
)
