package db

import (
	"context"
	"log"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func ConnectDB() *mongo.Database {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var (
		_MONGO_URI = os.Getenv("MONGO_URI")
		_DATABASE  = os.Getenv("DATABASE")
	)

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(_MONGO_URI))

	if err != nil {
		log.Fatal(err)
	}

	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		log.Fatal(err)
	}

	return client.Database(_DATABASE)
}
