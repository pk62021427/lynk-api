package configs

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/joho/godotenv"
)

func LoadEnv() {
	appENV := os.Getenv("APP_ENV")
	var envFile string
	var envName string

	rootPath := RootPath()

	switch appENV {
	case "PROD":
		envName = "PRODUCTION"
		envFile = rootPath + ".env.prod"
	case "STAGING":
		envName = "STAGING"
		envFile = rootPath + ".env.staging"
	default:
		envName = "DEVELOPMENT"
		envFile = rootPath + ".env.dev"
	}

	envKeys, err := ReadEnvKeys()

	if err != nil {
		log.Fatal(err)
	}

	if err := ValidEnv(envFile, envKeys); err != nil {
		log.Fatal(err)
	}
	fmt.Printf("\n🚀 Server is running on %v\n\n", envName)

}

func ReadEnvKeys() ([]string, error) {
	rootPath := RootPath()
	envExample := rootPath + ".env.example"
	content, err := ioutil.ReadFile(envExample)

	if err != nil {
		return nil, err
	}

	envKeysStr := strings.ReplaceAll(string(content), "=", "")
	envKeys := strings.Split(envKeysStr, "\n")

	filtered := []string{}

	for _, env := range envKeys {
		if env != "" {
			filtered = append(filtered, env)
		}
	}

	return filtered, nil
}

func ValidEnv(envFile string, envKeys []string) error {
	err := godotenv.Load(envFile)
	for _, env := range envKeys {
		if r := os.Getenv(env); r == "" {
			return fmt.Errorf("cannot load environment from %v", env)
		}
	}

	if err == nil {
		fmt.Printf("enviroments injected by %s", envFile)
	} else {
		fmt.Println("environments loaded from host")
	}
	return nil
}

func RootPath() string {
	_, b, _, _ := runtime.Caller(0)
	root := filepath.Join(filepath.Dir(b), "../..") + "/"
	return root
}
