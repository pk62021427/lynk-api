package configs

import (
	"lynk/src/handlers/guest"
	"lynk/src/handlers/recipe"
	"lynk/src/handlers/user"
	"lynk/src/handlers/users"
	"lynk/src/middleware"

	"github.com/gofiber/fiber/v2"
)

func SetupRoutes(app *fiber.App) {

	app.Get("/ping", func(c *fiber.Ctx) error {
		return c.JSON(fiber.Map{"message": "PONG!"})
	})

	apiv1 := app.Group("/api/v1")

	authH := user.NewAuthHandler()
	authRoute := apiv1.Group("/auth")

	authRoute.Post("/sign_up", authH.SignUp)
	authRoute.Post("/sign_in", authH.SignIn)

	userH := user.NewUserHandler()
	userRoute := apiv1.Group("/user")

	userRoute.Get("/me", middleware.JwtAuth(), userH.Me)
	userRoute.Patch("/", middleware.JwtAuth(), userH.Update)

	userRecipeH := user.NewRecipeHandler()
	userRecipeRoute := apiv1.Group("/user/recipes", middleware.JwtAuth())

	userRecipeRoute.Get("/", userRecipeH.Index)
	userRecipeRoute.Get("/:id", userRecipeH.Show)
	userRecipeRoute.Post("/", userRecipeH.Create)
	userRecipeRoute.Patch("/:id", userRecipeH.Update)
	userRecipeRoute.Delete("/:id", userRecipeH.Delete)

	guestH := guest.NewGuestHanlder()
	guestRoute := apiv1.Group("guest")

	guestRoute.Get("/recent", guestH.GetRecent)
	guestRoute.Get("/popular", guestH.GetPopular)
	guestRoute.Get("/suggest", guestH.GetSuggest)
	guestRoute.Get("/:id", guestH.GetRecipe)

	recipeH := recipe.NewRecipeHandler()
	recipeRoute := apiv1.Group("/recipes", middleware.JwtAuth())

	recipeRoute.Get("/", recipeH.Index)
	recipeRoute.Get("/:id", recipeH.Show)
	recipeRoute.Post("/like", recipeH.LikeRecipe)
	recipeRoute.Delete("/like/:id", recipeH.UnLikeRecipe)
	recipeRoute.Post("/comment", recipeH.CommentRecipe)
	recipeRoute.Delete("/comment/:id", recipeH.DeleteCommentRecipe)
	recipeRoute.Post("/review", recipeH.ReviewRecipe)

	usersH := users.NewUsersHanlder()
	usersRoute := apiv1.Group("/users")

	usersRoute.Get("/:id", usersH.FindUserById)
	usersRoute.Get("/:id/recipes", usersH.UserRecipes)
}
