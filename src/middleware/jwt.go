package middleware

import (
	"context"
	"lynk/src/models"
	"lynk/src/services"
	"time"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func JwtAuth() fiber.Handler {
	return func(c *fiber.Ctx) error {
		bearerToken := c.Get("Authorization")
		claims, err := services.ExtractTokenMetadata(bearerToken)

		if err != nil {
			return c.SendStatus(fiber.StatusUnauthorized)
		}

		user, err := getUser(claims["uid"].(string))

		if err != nil {
			return c.SendStatus(fiber.StatusUnauthorized)
		}

		if user == nil {
			return c.SendStatus(fiber.StatusUnauthorized)
		}

		c.Locals("user", user)

		return c.Next()
	}
}

func getUser(uid string) (*models.User, error) {
	objId, err := primitive.ObjectIDFromHex(uid)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err != nil {
		return nil, err
	}

	user := models.User{}
	res, err := user.FindOne(ctx, bson.M{"_id": objId})

	if err != nil {
		return nil, err
	}

	return res, nil

}
