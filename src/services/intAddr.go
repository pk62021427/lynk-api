package services

func ItoA(i int) *int {
	return &i
}

func AtoI(i *int) int {
	return *i
}
