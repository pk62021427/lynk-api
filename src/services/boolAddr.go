package services

func BoolAddr(b bool) *bool {
	return &b
}

func BoolVal(b *bool) bool {
	return *b
}
