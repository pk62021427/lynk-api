package services

import (
	"fmt"

	"github.com/go-playground/validator"
)

func Validate(data interface{}) *string {

	v := validator.New()

	if err := v.Struct(data); err != nil {
		for _, e := range err.(validator.ValidationErrors) {
			str := fmt.Sprintf("%v is required", e.Field())
			return &str
		}
	}

	return nil
}
