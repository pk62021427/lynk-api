package services

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
)

func PipeQuery(ctx context.Context, pipe mongo.Pipeline, col *mongo.Collection, results interface{}) error {
	cursor, err := col.Aggregate(ctx, pipe)

	if err != nil {
		return err
	}

	if err := cursor.All(ctx, results); err != nil {
		return err
	}

	return nil
}
