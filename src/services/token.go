package services

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

func CreateToken(uid string) (string, error) {
	atClaims := jwt.MapClaims{}
	atClaims["uid"] = uid
	atClaims["exp"] = time.Now().Add(time.Hour * 24 * 7).Unix()

	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString([]byte(os.Getenv("JWT_SECRET")))

	return token, err
}

func VerifyToken(bearerToken string) (*jwt.Token, error) {
	tokenString := extractToken(bearerToken)

	jwtToken, err := jwt.Parse(tokenString, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return "", fmt.Errorf("unexpected signing method: %v", t.Header["alg"])
		}

		return []byte(os.Getenv("JWT_SECRET")), nil
	})

	if err != nil {
		return nil, err
	}

	return jwtToken, nil
}

func ExtractTokenMetadata(bearerToken string) (jwt.MapClaims, error) {
	jwtToken, err := VerifyToken(bearerToken)

	if err != nil {
		return nil, err
	}

	claims, ok := jwtToken.Claims.(jwt.MapClaims)

	if !ok {
		return nil, fmt.Errorf("invalid token")
	}

	return claims, nil
}

func extractToken(bearerToken string) string {
	strArr := strings.Split(bearerToken, " ")

	if len(strArr) != 2 || strArr[0] != "Bearer" {
		return ""
	}
	return strArr[1]
}
