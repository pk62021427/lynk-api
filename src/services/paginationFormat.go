package services

import (
	"math"
)

func PaginationFormat(data interface{}, page, limit int, total int64) map[string]interface{} {

	return map[string]interface{}{
		"page":       page,
		"limit":      limit,
		"totalPages": math.Ceil(float64(total) / float64(limit)),
		"totalItem":  total,
		"data":       data,
	}
}
