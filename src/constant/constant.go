package constant

var (
	ViewScore    int = 1
	CommentScore int = 3
	CookingScore int = 5
	ReviewScore  int = 5
	LikeScore    int = 2
)
