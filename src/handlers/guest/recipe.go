package guest

import (
	"context"
	"lynk/src/constant"
	"lynk/src/models"
	"lynk/src/services"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type GuestHandler interface {
	GetRecipe(*fiber.Ctx) error
	GetRecent(c *fiber.Ctx) error
	GetPopular(c *fiber.Ctx) error
	GetSuggest(c *fiber.Ctx) error
}

type guestHandler struct {
	recipe models.Recipe
	rating models.Rating
}

func NewGuestHanlder() GuestHandler {
	return &guestHandler{models.Recipe{}, models.Rating{}}
}

func (h *guestHandler) GetRecipe(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	objId, err := primitive.ObjectIDFromHex(c.Params("id"))
	if err != nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"message": "กรุณาตรวจสอบข้อมูล"})
	}

	recipe, err := h.recipe.FindOne(ctx, mongo.Pipeline{
		{{"$match", bson.D{{"$and", []bson.M{{"_id": objId}, {"public": true}}}}}},
		{{"$lookup", bson.D{
			{"from", "ratings"},
			{"localField", "_id"},
			{"foreignField", "recipeId"},
			{"as", "rating"},
		}}},
		{{"$lookup", bson.D{
			{"from", "comments"},
			{"let", bson.D{{"recipe_id", "$_id"}}},
			{"pipeline", []bson.M{
				{"$match": bson.M{"$expr": bson.M{"$eq": []string{"$recipeId", "$$recipe_id"}}}},
				{"$lookup": bson.D{
					{"from", "users"},
					{"localField", "userId"},
					{"foreignField", "_id"},
					{"as", "user"},
				}},
			}},
			{"as", "comments"},
		}}},
		{{"$lookup", bson.D{
			{"from", "users"},
			{"localField", "userId"},
			{"foreignField", "_id"},
			{"as", "user"},
		}}},
		{{"$lookup", bson.D{
			{"from", "likes"},
			{"localField", "_id"},
			{"foreignField", "recipeId"},
			{"as", "likes"},
		}}},
		{{"$addFields", bson.D{
			{"likeCounts", bson.D{
				{"$size", "$likes"},
			}},
			{"comments", bson.D{
				{"$map", bson.D{
					{"input", "$comments"},
					{"as", "item"},
					{"in", bson.M{
						"_id":       "$$item._id",
						"text":      "$$item.text",
						"userId":    "$$item.userId",
						"recipeId":  "$$item.recipeId",
						"createdAt": "$$item.createdAt",
						"updatedAt": "$$item.updatedAt",
						"user": bson.M{
							"$arrayElemAt": []interface{}{"$$item.user", 0},
						},
					}},
				}},
			}},
		}}},
		{{"$project", bson.D{
			{"likes", 0},
		}}},
		{{"$unwind", bson.D{{"path", "$rating"}}}},
		{{"$unwind", bson.D{{"path", "$user"}}}},
	})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	if recipe == nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"message": "กรุณาตรวจสอบข้อมูล"})
	}

	*recipe.Rating.Score = *recipe.Rating.Score + constant.ViewScore
	*recipe.Rating.Views = *recipe.Rating.Views + 1

	if err := recipe.Rating.Update(ctx, recipe.Rating); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	return c.JSON(fiber.Map{"recipe": recipe})
}

func (h *guestHandler) GetRecent(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	page, err := strconv.Atoi(c.Query("page", "1"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "page must be number"})
	}

	limit, err := strconv.Atoi(c.Query("limit", "10"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "limit must be number"})
	}

	recipes, err := h.recipe.Find(ctx, mongo.Pipeline{
		{{"$match", bson.M{"public": true}}},
		{{"$lookup", bson.D{
			{"from", "ratings"},
			{"localField", "_id"},
			{"foreignField", "recipeId"},
			{"as", "rating"},
		}}},
		{{"$lookup", bson.D{
			{"from", "users"},
			{"localField", "userId"},
			{"foreignField", "_id"},
			{"as", "user"},
		}}},
		{{"$lookup", bson.D{
			{"from", "likes"},
			{"localField", "_id"},
			{"foreignField", "recipeId"},
			{"as", "likes"},
		}}},
		{{"$addFields", bson.D{
			{"likeCounts", bson.D{
				{"$size", "$likes"},
			}},
		}}},
		{{"$project", bson.D{
			{"likes", 0},
		}}},
		{{"$unwind", bson.D{{"path", "$rating"}}}},
		{{"$unwind", bson.D{{"path", "$user"}}}},
		{{"$sort", bson.D{{"createdAt", -1}}}},
		{{"$skip", (page - 1) * limit}},
		{{"$limit", limit}},
	})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	total, err := models.RecipeCol().CountDocuments(ctx, bson.M{"public": true})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	paginate := services.PaginationFormat(recipes, page, limit, total)

	return c.JSON(paginate)

}

func (h *guestHandler) GetPopular(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	page, err := strconv.Atoi(c.Query("page", "1"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "page must be number"})
	}

	limit, err := strconv.Atoi(c.Query("limit", "10"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "limit must be number"})
	}

	popular, err := h.recipe.Find(ctx, mongo.Pipeline{
		{{"$match", bson.M{"public": true}}},
		{{"$lookup", bson.D{
			{"from", "ratings"},
			{"localField", "_id"},
			{"foreignField", "recipeId"},
			{"as", "rating"},
		}}},
		{{"$lookup", bson.D{
			{"from", "users"},
			{"localField", "userId"},
			{"foreignField", "_id"},
			{"as", "user"},
		}}},
		{{"$lookup", bson.D{
			{"from", "likes"},
			{"localField", "_id"},
			{"foreignField", "recipeId"},
			{"as", "likes"},
		}}},
		{{"$addFields", bson.D{
			{"likeCounts", bson.D{
				{"$size", "$likes"},
			}},
		}}},
		{{"$project", bson.D{
			{"likes", 0},
		}}},
		{{"$unwind", bson.D{{"path", "$rating"}}}},
		{{"$unwind", bson.D{{"path", "$user"}}}},
		{{"$sort", bson.D{{"rating.score", -1}}}},
		{{"$skip", (page - 1) * limit}},
		{{"$limit", limit}},
	})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	total, err := models.RecipeCol().CountDocuments(ctx, bson.M{"public": true})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	paginate := services.PaginationFormat(popular, page, limit, total)

	return c.JSON(paginate)

}

func (h *guestHandler) GetSuggest(c *fiber.Ctx) error {

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	page, err := strconv.Atoi(c.Query("page", "1"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "page must be number"})
	}

	limit, err := strconv.Atoi(c.Query("limit", "10"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "limit must be number"})
	}

	suggest, err := h.recipe.Find(ctx, mongo.Pipeline{
		{{"$match", bson.M{"public": true}}},
		{{"$sample", bson.D{{"size", limit}}}},
		{{"$lookup", bson.D{
			{"from", "ratings"},
			{"localField", "_id"},
			{"foreignField", "recipeId"},
			{"as", "rating"},
		}}},
		{{"$lookup", bson.D{
			{"from", "users"},
			{"localField", "userId"},
			{"foreignField", "_id"},
			{"as", "user"},
		}}},
		{{"$lookup", bson.D{
			{"from", "likes"},
			{"localField", "_id"},
			{"foreignField", "recipeId"},
			{"as", "likes"},
		}}},
		{{"$addFields", bson.D{
			{"likeCounts", bson.D{
				{"$size", "$likes"},
			}},
		}}},
		{{"$project", bson.D{
			{"likes", 0},
		}}},
		{{"$unwind", bson.D{{"path", "$rating"}}}},
		{{"$unwind", bson.D{{"path", "$user"}}}},
		{{"$skip", (page - 1) * limit}},
		{{"$limit", limit}},
	})

	total, err := models.RecipeCol().CountDocuments(ctx, bson.M{"public": true})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	paginate := services.PaginationFormat(suggest, page, limit, total)

	return c.JSON(paginate)

}
