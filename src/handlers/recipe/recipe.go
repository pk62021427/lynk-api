package recipe

import (
	"context"
	"lynk/src/constant"
	"lynk/src/models"
	"lynk/src/services"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type RecipeHandler interface {
	DeleteCommentRecipe(c *fiber.Ctx) error
	UnLikeRecipe(c *fiber.Ctx) error
	LikeRecipe(c *fiber.Ctx) error
	CommentRecipe(c *fiber.Ctx) error
	ReviewRecipe(c *fiber.Ctx) error
	Index(c *fiber.Ctx) error
	Show(c *fiber.Ctx) error
}

type recipeHandler struct {
}

func NewRecipeHandler() RecipeHandler {
	return &recipeHandler{}
}

func (h *recipeHandler) Index(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	user := c.Locals("user").(*models.User)

	page, err := strconv.Atoi(c.Query("page", "1"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "page must be number"})
	}

	limit, err := strconv.Atoi(c.Query("limit", "10"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "limit must be number"})
	}

	recipes, err := models.Recipe{}.Find(ctx, mongo.Pipeline{
		{{"$match", bson.M{"public": true}}},
		{{"$lookup", bson.D{{"from", "likes"}, {"localField", "_id"}, {"foreignField", "recipeId"}, {"as", "like"}}}},
		{{"$lookup", bson.D{
			{"from", "comments"},
			{"localField", "_id"},
			{"foreignField", "recipeId"},
			{"as", "comments"},
		}}},
		{{"$addFields",
			bson.D{{"like",
				bson.D{{"$filter", bson.D{
					{"input", "$like"},
					{"as", "item"},
					{"cond", bson.D{{"$eq", []interface{}{"$$item.userId", user.ID}}}},
				}}},
			}},
		}},
		{{"$unwind", bson.D{{"path", "$like"}, {"preserveNullAndEmptyArrays", true}}}},
		{{"$skip", (page - 1) * limit}},
		{{"$limit", limit}},
	})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	total, err := models.RecipeCol().CountDocuments(ctx, bson.M{"public": true})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	paginate := services.PaginationFormat(recipes, page, limit, total)

	return c.JSON(paginate)
}

func (h *recipeHandler) Show(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	user := c.Locals("user").(*models.User)

	objId, err := primitive.ObjectIDFromHex(c.Params("id"))
	if err != nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"message": err.Error()})
	}

	recipe, err := models.Recipe{}.FindOne(ctx, mongo.Pipeline{
		{{"$match", bson.D{{"$and", []bson.M{{"_id": objId}, {"public": true}}}}}},
		{{"$lookup", bson.D{
			{"from", "ratings"},
			{"localField", "_id"},
			{"foreignField", "recipeId"},
			{"as", "rating"},
		}}},
		{{"$lookup", bson.D{
			{"from", "comments"},
			{"let", bson.D{{"recipe_id", "$_id"}}},
			{"pipeline", []bson.M{
				{"$match": bson.M{"$expr": bson.M{"$eq": []string{"$recipeId", "$$recipe_id"}}}},
				{"$lookup": bson.D{
					{"from", "users"},
					{"localField", "userId"},
					{"foreignField", "_id"},
					{"as", "user"},
				}},
			}},
			{"as", "comments"},
		}}},
		{{"$lookup", bson.D{
			{"from", "users"},
			{"localField", "userId"},
			{"foreignField", "_id"},
			{"as", "user"},
		}}},
		{{"$lookup", bson.D{
			{"from", "likes"},
			{"localField", "_id"},
			{"foreignField", "recipeId"},
			{"as", "likes"},
		}}},
		{{"$addFields",
			bson.D{
				{"like",
					bson.D{{"$filter", bson.D{
						{"input", "$likes"},
						{"as", "item"},
						{"cond", bson.D{{"$eq", []interface{}{"$$item.userId", user.ID}}}},
					}}},
				},
				{"likeCounts", bson.D{
					{"$size", "$likes"},
				}},
				{"comments", bson.D{
					{"$map", bson.D{
						{"input", "$comments"},
						{"as", "item"},
						{"in", bson.M{
							"_id":       "$$item._id",
							"text":      "$$item.text",
							"userId":    "$$item.userId",
							"recipeId":  "$$item.recipeId",
							"createdAt": "$$item.createdAt",
							"updatedAt": "$$item.updatedAt",
							"user": bson.M{
								"$arrayElemAt": []interface{}{"$$item.user", 0},
							},
						}},
					}},
				}},
			},
		}},
		{{"$unwind", bson.D{{"path", "$like"}, {"preserveNullAndEmptyArrays", true}}}},
		{{"$project", bson.D{
			{"likes", 0},
		}}},
		{{"$unwind", bson.D{{"path", "$rating"}}}},
		{{"$unwind", bson.D{{"path", "$user"}}}},
	})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	if recipe == nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"message": "กรุณาตรวจสอบข้อมูล"})
	}

	*recipe.Rating.Score = *recipe.Rating.Score + constant.ViewScore
	*recipe.Rating.Views = *recipe.Rating.Views + 1

	if err := recipe.Rating.Update(ctx, recipe.Rating); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	return c.JSON(fiber.Map{"recipe": recipe})
}

func (h *recipeHandler) LikeRecipe(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	user := c.Locals("user").(*models.User)

	objId, err := primitive.ObjectIDFromHex(c.Query("id"))
	if err != nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"message": "กรุณาตรวจสอบข้อมูล"})
	}

	validRecipe, err := models.Recipe{}.Find(ctx, mongo.Pipeline{
		{{"$match", bson.D{{"_id", objId}}}},
	})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	if validRecipe == nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"message": "กรุณาตรวจสอบข้อมูล"})
	}

	existLike, err := models.Like{}.FindOne(ctx, mongo.Pipeline{
		{{"$lookup", bson.D{{"from", "recipes"}, {"localField", "recipeId"}, {"foreignField", "_id"}, {"as", "recipe"}}}},
		{{"$unwind", bson.D{{"path", "$recipe"}}}},
		{{"$match", bson.D{{"$and", []bson.M{{"recipe.public": true}, {"recipe._id": objId}, {"userId": user.ID}}}}}},
	})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	if existLike != nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"message": "กรุณาตรวจสอบข้อมูล"})
	}

	like := &models.Like{
		UserID:   user.ID,
		RecipeID: objId,
	}

	if err := like.Create(ctx); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	rating, err := models.Rating{}.FindOne(ctx, mongo.Pipeline{
		{{"$match", bson.D{{"recipeId", objId}}}},
	})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	*rating.Score += constant.LikeScore

	if err := rating.Update(ctx, nil); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	return c.Status(fiber.StatusCreated).JSON(fiber.Map{"like": like})
}

func (h *recipeHandler) CommentRecipe(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	user := c.Locals("user").(*models.User)

	objId, err := primitive.ObjectIDFromHex(c.Query("id"))

	if err != nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"message": "กรูณาตรวจสอบข้อมูล"})
	}

	exist, err := models.Recipe{}.FindOne(ctx, mongo.Pipeline{
		{{"$match", bson.D{{"$and", []bson.M{{"_id": objId}, {"public": true}}}}}},
	})

	if err != nil {
		return c.Status(fiber.StatusBadGateway).JSON(fiber.Map{"error": err.Error()})
	}

	if exist == nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"message": "กรูณาตรวจสอบข้อมูล"})
	}

	type request struct {
		Text string `json:"text" validate:"required"`
	}

	var body request

	if err := c.BodyParser(&body); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	if err := services.Validate(body); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": err})
	}

	comment := &models.Comment{
		UserID:   user.ID,
		RecipeID: objId,
		Text:     body.Text,
	}

	if err := comment.Create(ctx); err != nil {
		return c.Status(fiber.StatusBadGateway).JSON(fiber.Map{"error": err.Error()})
	}

	rating, err := models.Rating{}.FindOne(ctx, mongo.Pipeline{
		{{"$match", bson.D{{"recipeId", objId}}}},
	})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	*rating.Score += constant.LikeScore

	if err := rating.Update(ctx, nil); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	return c.Status(fiber.StatusCreated).JSON(fiber.Map{"comment": comment})
}

func (h *recipeHandler) DeleteCommentRecipe(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	objId, err := primitive.ObjectIDFromHex(c.Params("id"))
	if err != nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"message": "กรุณาขรวจสอบข้อมูล"})
	}

	user := c.Locals("user").(*models.User)

	comment, err := models.Comment{}.FindOne(ctx, mongo.Pipeline{
		{{"$match", bson.D{{"$and", []bson.M{{"_id": objId}, {"userId": user.ID}}}}}},
	})

	if err != nil {
		return c.Status(fiber.StatusBadGateway).JSON(fiber.Map{"error": err.Error()})
	}

	if comment == nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"message": "กรุณาขรวจสอบข้อมูล"})
	}

	if err := comment.Delete(ctx); err != nil {
		return c.Status(fiber.StatusBadGateway).JSON(fiber.Map{"error": err.Error()})
	}

	return c.SendStatus(fiber.StatusNoContent)
}

func (h *recipeHandler) ReviewRecipe(c *fiber.Ctx) error {
	return nil
}

func (h *recipeHandler) UnLikeRecipe(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	user := c.Locals("user").(*models.User)

	objId, err := primitive.ObjectIDFromHex(c.Params("id"))
	if err != nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"message": "กรุณาขรวจสอบข้อมูล"})
	}

	like, err := models.Like{}.FindOne(ctx, mongo.Pipeline{
		{{"$match", bson.D{{"$and", []bson.M{{"_id": objId}, {"userId": user.ID}}}}}},
	})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	if like == nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"message": "กรุณาตรวจสอบข้อมูล"})
	}

	if err := like.Delete(ctx); err != nil {

		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	return c.SendStatus(fiber.StatusNoContent)
}
