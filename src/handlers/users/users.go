package users

import (
	"context"
	"lynk/src/models"
	"lynk/src/services"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type UsersHandler interface {
	FindUserById(c *fiber.Ctx) error
	UserRecipes(c *fiber.Ctx) error
}

type usersHandler struct {
}

func NewUsersHanlder() UsersHandler {
	return &usersHandler{}
}

func (h *usersHandler) FindUserById(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	objId, err := primitive.ObjectIDFromHex(c.Params("id"))

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	user, err := models.User{}.FindOne(ctx, bson.M{"_id": objId})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	if user == nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"message": "กรุณาตรวจสอบข้อมูล"})
	}

	return c.JSON(user)
}

func (h *usersHandler) UserRecipes(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	objId, err := primitive.ObjectIDFromHex(c.Params("id"))

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	page, err := strconv.Atoi(c.Query("page", "1"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "page must be number"})
	}

	limit, err := strconv.Atoi(c.Query("limit", "10"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "limit must be number"})
	}

	recipes, err := models.Recipe{}.Find(ctx, mongo.Pipeline{
		{{"$match", bson.D{
			{"$and", []bson.M{{"userId": objId}, {"public": true}}},
		}}},
		{{"$lookup", bson.D{
			{"from", "ratings"},
			{"localField", "_id"},
			{"foreignField", "recipeId"},
			{"as", "rating"},
		}}},
		{{"$lookup", bson.D{
			{"from", "comments"},
			{"localField", "_id"},
			{"foreignField", "recipeId"},
			{"as", "comments"},
		}}},
		{{"$lookup", bson.D{
			{"from", "likes"},
			{"localField", "_id"},
			{"foreignField", "recipeId"},
			{"as", "likes"},
		}}},
		{{"$addFields",
			bson.D{
				{"likeCounts", bson.D{
					{"$size", "$likes"},
				}},
			},
		}},
		{{"$project", bson.D{
			{"likes", 0},
		}}},
		{{"$unwind", bson.D{{"path", "$rating"}}}},
		{{"$skip", (page - 1) * limit}},
		{{"$limit", limit}},
		{{"$sort", bson.D{{"createdAt", -1}}}},
	})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	size, err := models.RecipeCol().CountDocuments(ctx, bson.D{{
		"$and", []bson.M{{"userId": objId}, {"public": true}},
	}})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	paginate := services.PaginationFormat(recipes, page, limit, size)

	return c.JSON(paginate)
}
