package user

import (
	"context"
	"lynk/src/models"
	"lynk/src/services"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type RecipeHandler interface {
	Index(c *fiber.Ctx) error
	Show(c *fiber.Ctx) error
	Create(c *fiber.Ctx) error
	Update(c *fiber.Ctx) error
	Delete(c *fiber.Ctx) error
}

type recipeHandler struct {
	model models.Recipe
	col   *mongo.Collection
}

func NewRecipeHandler() RecipeHandler {
	return &recipeHandler{
		models.Recipe{},
		models.RecipeCol(),
	}
}

// Index
func (r *recipeHandler) Index(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	user := c.Locals("user").(*models.User)
	page, err := strconv.Atoi(c.Query("page", "1"))
	status := c.Query("status", "private")

	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "expected page to be number"})
	}
	limit, err := strconv.Atoi(c.Query("limit", "10"))
	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "expected limit to be number"})
	}

	var matchStage bson.D

	if status == "public" {
		matchStage = bson.D{{"$match", bson.D{{"$and", []bson.M{{"userId": user.ID}, {"public": true}}}}}}
	} else {
		matchStage = bson.D{{"$match", bson.M{"userId": user.ID}}}
	}

	recipes, err := r.model.Find(ctx, mongo.Pipeline{
		matchStage,
		{{"$lookup", bson.D{
			{"from", "ratings"},
			{"localField", "_id"},
			{"foreignField", "recipeId"},
			{"as", "rating"},
		}}},
		{{"$lookup", bson.D{
			{"from", "comments"},
			{"localField", "_id"},
			{"foreignField", "recipeId"},
			{"as", "comments"},
		}}},
		{{"$lookup", bson.D{
			{"from", "likes"},
			{"localField", "_id"},
			{"foreignField", "recipeId"},
			{"as", "likes"},
		}}},
		{{"$addFields",
			bson.D{
				{"likeCounts", bson.D{
					{"$size", "$likes"},
				}},
			},
		}},
		{{"$project", bson.D{
			{"likes", 0},
		}}},
		{{"$unwind", bson.D{{"path", "$rating"}}}},
		{{"$skip", (page - 1) * limit}},
		{{"$limit", limit}},
		{{"$sort", bson.D{{"createdAt", -1}}}},
	})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	size, err := models.RecipeCol().CountDocuments(ctx, bson.M{"userId": user.ID})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	paginate := services.PaginationFormat(recipes, page, limit, size)

	return c.JSON(paginate)
}

// Show
func (r *recipeHandler) Show(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	user := c.Locals("user").(*models.User)

	objId, err := primitive.ObjectIDFromHex(c.Params("id"))

	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "กรุณาตรวจสอบข้อมูล"})
	}

	andStage := []bson.M{bson.M{"userId": user.ID}, bson.M{"_id": objId}}
	matchStage := bson.D{{"$match", bson.D{{"$and", andStage}}}}

	recipe, err := models.Recipe{}.FindOne(ctx, mongo.Pipeline{matchStage})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	if recipe == nil {
		return c.SendStatus(fiber.StatusNotFound)
	}

	return c.JSON(fiber.Map{"recipe": recipe})
}

// Create
func (r *recipeHandler) Create(c *fiber.Ctx) error {
	type request struct {
		Refer       string               `json:"refer"`
		Title       string               `json:"title" validate:"required"`
		Image       string               `json:"image" validate:"required"`
		Amount      *int                 `json:"amount" validate:"required"`
		Duration    *int                 `json:"duration" validate:"required"`
		Ingredients *[]models.Ingredient `json:"ingredients" validate:"required"`
		Steps       *[]models.Step       `json:"steps" validate:"required"`
		Public      *bool                `json:"public" validate:"required"`
	}

	var body request

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	user := c.Locals("user").(*models.User)

	if err := c.BodyParser(&body); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	if err := services.Validate(body); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": err})
	}

	var referId *primitive.ObjectID = nil

	if body.Refer != "" {
		objId, err := primitive.ObjectIDFromHex(body.Refer)
		if err != nil {
			return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "กรูณาตรวจสอบข้อมูล"})
		}
		referId = &objId
	}

	recipe := &models.Recipe{
		Refer:       referId,
		UserID:      user.ID,
		Title:       body.Title,
		Image:       body.Image,
		Duration:    body.Duration,
		Amount:      body.Amount,
		Ingredients: body.Ingredients,
		Steps:       body.Steps,
		Public:      body.Public,
	}

	if err := recipe.Create(ctx); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	rating := &models.Rating{
		RecipeID: recipe.ID,
		Score:    services.ItoA(0),
		Views:    services.ItoA(0),
	}

	if err := rating.Create(ctx); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	return c.Status(fiber.StatusCreated).JSON(fiber.Map{"recipe": recipe})

}

// Update
func (r *recipeHandler) Update(c *fiber.Ctx) error {
	type request struct {
		Title       string `json:"title"`
		Image       string `json:"image"`
		Amount      *int   `json:"amount"`
		Duration    *int   `json:"duration"`
		Ingredients *[]models.Ingredient
		Steps       *[]models.Step
		Public      *bool `json:"public"`
	}

	var body request

	user := c.Locals("user").(*models.User)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := c.BodyParser(&body); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	objId, err := primitive.ObjectIDFromHex(c.Params("id"))

	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "กรุณาตรวจสอบข้อมูล"})
	}

	andStage := []bson.M{bson.M{"_id": objId}, bson.M{"userId": user.ID}}
	matchStage := bson.D{{"$match", bson.D{{"$and", andStage}}}}
	pipe := mongo.Pipeline{matchStage}

	recipe, err := models.Recipe{}.FindOne(ctx, pipe)

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	if recipe == nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "กรุณาตรวจสอบข้อมูล"})
	}

	payload := models.Recipe{
		Title:       body.Title,
		Image:       body.Image,
		Amount:      body.Amount,
		Duration:    body.Duration,
		Ingredients: body.Ingredients,
		Steps:       body.Steps,
		Public:      body.Public,
	}

	if err := recipe.Update(ctx, &payload); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	return c.JSON(fiber.Map{"recipe": recipe})
}

// Delete
func (r *recipeHandler) Delete(c *fiber.Ctx) error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	user := c.Locals("user").(*models.User)

	objId, err := primitive.ObjectIDFromHex(c.Params("id"))

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	andStage := []bson.M{bson.M{"_id": objId}, bson.M{"userId": user.ID}}
	matchStage := bson.D{{"$match", bson.D{{"$and", andStage}}}}
	pipe := mongo.Pipeline{matchStage}
	recipe, err := models.Recipe{}.FindOne(ctx, pipe)

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	if recipe == nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "กรุณาตรวจสอบข้อมูล"})
	}

	recipe.Delete(ctx)

	return c.JSON(fiber.Map{"success": true})
}
