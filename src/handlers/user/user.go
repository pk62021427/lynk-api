package user

import (
	"context"
	"lynk/src/models"
	"time"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/mongo"
)

type UserHandler interface {
	Me(c *fiber.Ctx) error
	Update(c *fiber.Ctx) error
}

type userHandler struct {
	col *mongo.Collection
}

func NewUserHandler() UserHandler {
	return &userHandler{col: models.UserCol()}
}

func (u userHandler) Me(c *fiber.Ctx) error {
	user := c.Locals("user").(*models.User)
	return c.JSON(fiber.Map{"user": user})
}

func (u userHandler) Update(c *fiber.Ctx) error {
	type request struct {
		FirstName string `json:"firstName"`
		LastName  string `json:"lastName"`
		Avatar    string `json:"Avatar"`
	}

	var body request

	user := c.Locals("user").(*models.User)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := c.BodyParser(&body); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	payload := models.User{FirstName: body.FirstName, LastName: body.LastName}

	if err := user.Update(ctx, &payload); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"erorr": err.Error()})
	}

	return c.JSON(fiber.Map{"user": user})
}
