package user

import (
	"context"
	"lynk/src/models"
	"lynk/src/services"
	"time"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type AuthHandler interface {
	SignIn(c *fiber.Ctx) error
	SignUp(c *fiber.Ctx) error
}

type authHandler struct {
	col *mongo.Collection
}

func NewAuthHandler() AuthHandler {
	return &authHandler{col: models.UserCol()}
}

func (u authHandler) SignIn(c *fiber.Ctx) error {
	type request struct {
		Email    string `json:"email"`
		Password string `json:"password"`
		Username string `json:"username"`
	}

	var body request

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := c.BodyParser(&body); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	user := models.User{}
	res, err := user.FindOne(ctx, bson.D{{"$or", []bson.M{bson.M{"email": body.Email}, bson.M{"username": body.Username}}}})

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	if res == nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "กรุณาตรวจสอบข้อมูล"})
	}

	if ok := services.ComparePassword(body.Password, res.Password); !ok {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "กรุณาตรวจสอบข้อมูล"})
	}

	token, err := services.CreateToken(res.ID.Hex())

	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	return c.JSON(fiber.Map{
		"user":  res,
		"token": token,
	})

}
func (u authHandler) SignUp(c *fiber.Ctx) error {

	type request struct {
		Email           string `json:"email" validate:"required,email"`
		Password        string `json:"password" validate:"required"`
		ConfirmPassword string `json:"confirmPassword" validate:"required"`
		Username        string `json:"username" validate:"required"`
		FirstName       string `json:"firstName" validate:"required"`
		LastName        string `json:"lastName" validate:"required"`
	}

	var body request

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := c.BodyParser(&body); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	if err := services.Validate(body); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": err})
	}

	if body.Password != body.ConfirmPassword {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "กรุณาตรวจสอบรหัสผ่าน"})
	}

	// res, err := existUser.FindOne(ctx, bson.D{{"$or", []bson.M{bson.M{"email": body.Email}, bson.M{"username": body.Username}}}})

	// if err != nil {
	// 	return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	// }

	// if res != nil {
	// 	return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{"message": "กรุณาตรวจสอบข้อมูล"})
	// }

	user := models.User{
		Email:     body.Email,
		Password:  body.Password,
		Username:  body.Username,
		FirstName: body.FirstName,
		LastName:  body.LastName,
	}

	if err := user.Create(ctx); err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"error": err.Error()})
	}

	return c.Status(fiber.StatusCreated).JSON(fiber.Map{"success": true})
}
