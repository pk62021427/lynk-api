package models

import (
	"context"
	"lynk/src/db"
	"lynk/src/services"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Like struct {
	ID        primitive.ObjectID `json:"_id" bson:"_id"`
	UserID    primitive.ObjectID `json:"userId" bson:"userId"`
	User      *User              `json:"user,omitempty" bson:"user,omitempty"`
	RecipeID  primitive.ObjectID `json:"recipeId" bson:"recipeId"`
	Recipe    *Recipe            `json:"recipe,omitempty" bson:"recipe,omitempty"`
	CreatedAt primitive.DateTime `json:"createdAt" bson:"createdAt"`
	UpdatedAt primitive.DateTime `json:"updatedAt" bson:"updatedAt"`
}

var _LikeCol = "likes"

func LikeCol() *mongo.Collection {
	return db.ConnectDB().Collection(_LikeCol)
}

func (l Like) Find(ctx context.Context, pipe mongo.Pipeline) ([]Like, error) {
	var result []Like
	err := services.PipeQuery(ctx, pipe, LikeCol(), &result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (l Like) FindOne(ctx context.Context, pipe mongo.Pipeline) (*Like, error) {
	result, err := l.Find(ctx, pipe)
	if err != nil {
		return nil, err
	}

	if len(result) == 0 {
		return nil, nil
	}

	return &result[0], nil

}

func (l *Like) Create(ctx context.Context) error {
	l.ID = primitive.NewObjectID()
	l.CreatedAt = primitive.NewDateTimeFromTime(time.Now())
	l.UpdatedAt = l.CreatedAt

	client := LikeCol()
	if _, err := client.InsertOne(ctx, l); err != nil {
		return err
	}

	return nil
}

func (l *Like) Delete(ctx context.Context) error {

	client := LikeCol()
	if _, err := client.DeleteOne(ctx, bson.M{"_id": l.ID}); err != nil {
		return err
	}

	return nil
}
