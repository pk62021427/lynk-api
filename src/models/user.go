package models

import (
	"context"
	"fmt"
	"lynk/src/db"
	"lynk/src/services"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type User struct {
	ID        primitive.ObjectID `json:"_id" bson:"_id"`
	Email     string             `json:"email" bson:"email"`
	Username  string             `json:"username" bson:"username"`
	Password  string             `json:"-" bson:"password"`
	Avatar    string             `json:"avatar" bson:"avatar"`
	FirstName string             `json:"firstName" bson:"firstName"`
	LastName  string             `json:"lastName" bson:"lastName"`
	CreatedAt primitive.DateTime `json:"createdAt" bson:"createdAt"`
	UpdatedAt primitive.DateTime `json:"updatedAt" bson:"updatedAt"`
}

const _USER_COL = "users"

// Collection
func UserCol() *mongo.Collection {

	db := db.ConnectDB()

	client := db.Collection(_USER_COL)
	client.Indexes().CreateMany(context.TODO(), []mongo.IndexModel{
		{
			Keys:    bson.D{{"email", 1}},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys:    bson.D{{"username", 1}},
			Options: options.Index().SetUnique(true),
		},
	})
	return client

}

func usernameFormat(s *string) {
	f := strings.Replace(*s, "@", "", -1)
	*s = fmt.Sprintf("@%s", f)
}

// Find
func (u *User) Find(ctx context.Context, pipe mongo.Pipeline) ([]User, error) {
	var data []User
	err := services.PipeQuery(ctx, pipe, UserCol(), &data)

	if err != nil {
		return nil, err
	}

	return data, nil
}

// FindOne
func (u User) FindOne(ctx context.Context, filter interface{}) (*User, error) {
	pipe := mongo.Pipeline{bson.D{{"$match", filter}}}

	res, err := u.Find(ctx, pipe)
	if err != nil {
		return nil, err
	}

	if len(res) == 0 {
		return nil, nil
	}

	return &res[0], nil
}

// Create
func (u *User) Create(ctx context.Context) error {
	hash, err := services.HashPassword(u.Password)

	client := UserCol()

	if err != nil {
		return err
	}

	u.ID = primitive.NewObjectID()
	u.Password = hash
	u.CreatedAt = primitive.NewDateTimeFromTime(time.Now())
	u.UpdatedAt = u.CreatedAt

	usernameFormat(&u.Username)

	if _, err := client.InsertOne(ctx, u); err != nil {
		return err
	}

	return nil
}

// Update
func (u *User) Update(ctx context.Context, data *User) error {
	client := UserCol()

	if data != nil {

		if data.FirstName != "" {
			u.FirstName = data.FirstName
		}

		if data.LastName != "" {
			u.LastName = data.LastName
		}

		if data.Password != "" {
			hash, err := services.HashPassword(data.Password)
			if err != nil {
				return err
			}

			u.Password = hash
		}

		if data.Avatar != "" {
			u.Avatar = data.Avatar
		}
	}

	u.UpdatedAt = primitive.NewDateTimeFromTime(time.Now())

	if _, err := client.UpdateOne(ctx, bson.M{"_id": u.ID}, bson.D{{"$set", u}}); err != nil {
		return err
	}

	return nil
}
