package models

import (
	"context"
	"lynk/src/db"
	"lynk/src/services"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Recipe struct {
	ID          primitive.ObjectID  `json:"_id" bson:"_id"`
	UserID      primitive.ObjectID  `json:"userId" bson:"userId"`
	User        *User               `json:"user,omitempty" bson:"user,omitempty"`
	Refer       *primitive.ObjectID `json:"refer" bson:"refer,omitempty"`
	Title       string              `json:"title" bson:"title"`
	Image       string              `json:"image" bson:"image"`
	Amount      *int                `json:"amount" bson:"amount"`
	Duration    *int                `json:"duration" bson:"duration"`
	Ingredients *[]Ingredient       `json:"ingredients" bson:"ingredients"`
	Steps       *[]Step             `json:"steps" bson:"steps"`
	Public      *bool               `json:"public" bson:"public"`
	CreatedAt   primitive.DateTime  `json:"createdAt" bson:"createdAt"`
	UpdatedAt   primitive.DateTime  `json:"updatedAt" bson:"updatedAt"`
	Rating      *Rating             `json:"rating,omitempty" bson:"rating,omitempty"`
	Like        *Like               `json:"like,omitempty" bson:"like,omitempty"`
	LikeCounts  *int                `json:"likeCounts,omitempty" bson:"likeCounts,omitempty"`
	Comments    *[]Comment          `json:"comments,omitempty" bson:"comments,omitempty"`
}

type Ingredient struct {
	Name   string   `json:"name" bson:"name"`
	Image  string   `json:"image" bson:"image,omitempty"`
	Amount *float64 `json:"amount" bson:"amount"`
	Unit   string   `json:"unit" bson:"unit"`
}

type Step struct {
	Text   string   `json:"text" bson:"text"`
	Images []string `json:"images" bson:"images"`
}

const _RECIPE_COL = "recipes"

// Collection
func RecipeCol() *mongo.Collection {
	client := db.ConnectDB()
	return client.Collection(_RECIPE_COL)
}

// Find
func (r Recipe) Find(ctx context.Context, pipe mongo.Pipeline) ([]Recipe, error) {
	var data []Recipe
	err := services.PipeQuery(ctx, pipe, RecipeCol(), &data)

	if err != nil {
		return nil, err
	}

	return data, nil
}

// Find One
func (r Recipe) FindOne(ctx context.Context, pipe mongo.Pipeline) (*Recipe, error) {
	res, err := r.Find(ctx, pipe)
	if err != nil {
		return nil, err
	}

	if len(res) == 0 {
		return nil, nil
	}

	return &res[0], nil
}

// Create
func (r *Recipe) Create(ctx context.Context) error {
	client := RecipeCol()

	r.ID = primitive.NewObjectID()
	r.CreatedAt = primitive.NewDateTimeFromTime(time.Now())
	r.UpdatedAt = r.CreatedAt

	_, err := client.InsertOne(ctx, r)
	return err
}

// Update
func (r *Recipe) Update(ctx context.Context, data *Recipe) error {
	client := RecipeCol()

	if data != nil {
		if data.Title != "" {
			r.Title = data.Title
		}

		if data.Amount != nil {
			r.Amount = data.Amount
		}

		if data.Duration != nil {
			r.Duration = data.Duration
		}

		if data.Ingredients != nil {
			r.Ingredients = data.Ingredients
		}

		if data.Steps != nil {
			r.Steps = data.Steps
		}

		if data.Public != nil {
			r.Public = data.Public
		}

		if data.Image != "" {
			r.Image = data.Image
		}

	}

	r.UpdatedAt = primitive.NewDateTimeFromTime(time.Now())

	if _, err := client.UpdateOne(ctx, bson.M{"_id": r.ID}, bson.D{{"$set", r}}); err != nil {
		return err
	}

	return nil
}

// Delete
func (r Recipe) Delete(ctx context.Context) error {
	client := RecipeCol()

	if _, err := client.DeleteOne(ctx, bson.M{"_id": r.ID}); err != nil {
		return err
	}

	return nil
}
