package models

import (
	"context"
	"lynk/src/db"
	"lynk/src/services"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Comment struct {
	ID        primitive.ObjectID `json:"_id" bson:"_id"`
	Text      string             `json:"text" bson:"text"`
	UserID    primitive.ObjectID `json:"userId" bson:"userId"`
	User      *User              `json:"user,omitempty" bson:"user,omitempty"`
	RecipeID  primitive.ObjectID `json:"recipeId" bson:"recipeId"`
	Recipe    *Recipe            `json:"recipe,omitempty" bson:"recipe,omitempty"`
	CreatedAt primitive.DateTime `json:"createdAt" bson:"createdAt"`
	UpdatedAt primitive.DateTime `json:"updatedAt" bson:"updatedAt"`
}

func CommentCol() *mongo.Collection {
	return db.ConnectDB().Collection("comments")
}

func (c Comment) Find(ctx context.Context, pipe mongo.Pipeline) ([]Comment, error) {
	var data []Comment

	err := services.PipeQuery(ctx, pipe, CommentCol(), &data)

	if err != nil {
		return nil, err
	}
	return data, nil
}

func (c Comment) FindOne(ctx context.Context, pipe mongo.Pipeline) (*Comment, error) {
	res, err := c.Find(ctx, pipe)

	if err != nil {
		return nil, err
	}

	if len(res) == 0 {
		return nil, nil
	}

	return &res[0], nil
}

func (c *Comment) Create(ctx context.Context) error {
	client := CommentCol()

	c.ID = primitive.NewObjectID()
	c.CreatedAt = primitive.NewDateTimeFromTime(time.Now())
	c.UpdatedAt = c.CreatedAt

	_, err := client.InsertOne(ctx, c)

	if err != nil {
		return err
	}

	return nil

}

func (c *Comment) Update(ctx context.Context, data *Comment) error {

	client := CommentCol()

	if data != nil {
		if data.Text != "" {
			c.Text = data.Text
		}
	}

	c.UpdatedAt = primitive.NewDateTimeFromTime(time.Now())

	_, err := client.UpdateOne(ctx, bson.M{"_id": c.ID}, bson.D{{"$set", c}})

	if err != nil {
		return err
	}

	return nil
}

func (c *Comment) Delete(ctx context.Context) error {
	client := CommentCol()

	_, err := client.DeleteOne(ctx, bson.M{"_id": c.ID})
	if err != nil {
		return err
	}

	return nil
}
