package models

import (
	"context"
	"lynk/src/db"
	"lynk/src/services"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Rating struct {
	ID        primitive.ObjectID `json:"_id" bson:"_id"`
	RecipeID  primitive.ObjectID `json:"recipeId" bson:"recipeId"`
	Recipe    *Recipe            `json:"recipe,omitempty" bson:"recipe,omitempty"`
	Score     *int               `json:"score" bson:"score"`
	Views     *int               `json:"views" bson:"views"`
	CreatedAt primitive.DateTime `json:"createdAt" bson:"createdAt"`
	UpdatedAt primitive.DateTime `json:"updatedAt" bson:"updatedAt"`
}

func RatingCol() *mongo.Collection {
	return db.ConnectDB().Collection("ratings")
}

func (r Rating) Find(ctx context.Context, pipe mongo.Pipeline) ([]Rating, error) {
	var data []Rating

	err := services.PipeQuery(ctx, pipe, RatingCol(), &data)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (r Rating) FindOne(ctx context.Context, pipe mongo.Pipeline) (*Rating, error) {

	res, err := r.Find(ctx, pipe)
	if err != nil {
		return nil, err
	}

	if len(res) == 0 {
		return nil, nil
	}

	return &res[0], nil
}

func (r *Rating) Create(ctx context.Context) error {
	client := RatingCol()

	r.ID = primitive.NewObjectID()
	r.CreatedAt = primitive.NewDateTimeFromTime(time.Now())
	r.Score = services.ItoA(0)
	r.Views = services.ItoA(0)
	r.UpdatedAt = r.CreatedAt

	_, err := client.InsertOne(ctx, r)
	if err != nil {
		return err
	}

	return nil
}

func (r *Rating) Update(ctx context.Context, data *Rating) error {
	client := RatingCol()

	if data != nil {
		if data.Score != nil {
			r.Score = data.Score
		}
		if data.Views != nil {
			r.Views = data.Views
		}
	}

	r.UpdatedAt = primitive.NewDateTimeFromTime(time.Now())

	_, err := client.UpdateOne(ctx, bson.M{"_id": r.ID}, bson.D{{"$set", r}})
	if err != nil {
		return err
	}

	return nil
}

func (r *Rating) Delete(ctx context.Context) error {
	client := RatingCol()
	_, err := client.DeleteOne(ctx, bson.M{"_id": r.ID})
	if err != nil {
		return err
	}
	return nil
}

func (r *Rating) ClearScore(ctx context.Context) error {
	z := 0
	err := r.Update(ctx, &Rating{Score: &z})

	if err != nil {
		return err
	}

	return nil

}

func ratingLookup() bson.D {
	return bson.D{{"$lookup",
		bson.D{
			{"from", "recipes"},
			{"as", "recipe"},
			{"let", bson.D{{"recipe_id", "$recipeId"}}},
			{"pipeline", []bson.M{
				{"$match": bson.M{"$expr": bson.M{"$eq": []string{"$_id", "$$recipe_id"}}}},
				{"$lookup": bson.D{
					{"from", "users"},
					{"localField", "userId"},
					{"foreignField", "_id"},
					{"as", "user"},
				}},
			}},
		},
	}}
}

func (r Rating) Recent(ctx context.Context, page, limit int) ([]Rating, error) {
	result, err := r.Find(ctx, mongo.Pipeline{
		ratingLookup(),
		bson.D{{"$unwind", "$recipe"}},
		bson.D{{"$unwind", "$recipe.user"}},
		bson.D{{"$match", bson.D{{"recipe.public", true}}}},
		bson.D{{"$sort", bson.D{{"recipe._id", -1}}}},
		bson.D{{"$skip", (page - 1) * limit}},
		bson.D{{"$limit", limit}},
	})
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (r Rating) Popular(ctx context.Context, page, limit int) ([]Rating, error) {
	result, err := r.Find(ctx, mongo.Pipeline{
		ratingLookup(),
		bson.D{{"$unwind", "$recipe"}},
		bson.D{{"$unwind", "$recipe.user"}},
		bson.D{{"$match", bson.D{{"recipe.public", true}}}},
		bson.D{{"$sort", bson.D{{"score", -1}}}},
		bson.D{{"$skip", (page - 1) * limit}},
		bson.D{{"$limit", limit}},
	})
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (r Rating) Suggest(ctx context.Context, page, limit int) ([]Rating, error) {

	size, err := RatingCol().EstimatedDocumentCount(ctx)
	if err != nil {
		return nil, err
	}

	result, err := r.Find(ctx, mongo.Pipeline{
		ratingLookup(),
		bson.D{{"$unwind", "$recipe"}},
		bson.D{{"$unwind", "$recipe.user"}},
		bson.D{{"$match", bson.D{{"recipe.public", true}}}},
		bson.D{{"$sample", bson.D{{"size", size}}}},
		bson.D{{"$skip", (page - 1) * limit}},
		bson.D{{"$limit", limit}},
	})

	if err != nil {
		return nil, err
	}

	return result, nil
}
