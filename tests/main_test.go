package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"lynk/src/configs"
	"lynk/src/models"
	"lynk/src/services"
	"os"
	"testing"

	"github.com/bxcodec/faker/v3"
	"go.mongodb.org/mongo-driver/bson"
)

func Test1(t *testing.T) {
	pwdTable := []struct {
		val    string
		newVal string
		expect bool
	}{
		{"123", "123", true},
		{"123", "1234", false},
		{"111", "111", true},
	}

	for _, p := range pwdTable {
		hash, _ := services.HashPassword(p.val)
		if p.expect != services.ComparePassword(p.newVal, hash) {
			t.Error("error password")
		}
	}

	token, _ := services.CreateToken("123")
	if _, err := services.VerifyToken(token); err == nil {
		t.Error("error on token")
	}
	if _, err := services.VerifyToken(fmt.Sprintf("Bearer %s", token)); err != nil {
		t.Error("error on token")
	}
	if _, err := services.VerifyToken(fmt.Sprintf("BearerT %s", token)); err == nil {
		t.Error("error on token")
	}
	if _, err := services.VerifyToken(fmt.Sprintf("Bearer %s5", token)); err == nil {
		t.Error("error on token")
	}
}

func TestCreate(t *testing.T) {
	configs.LoadEnv()

	userTable := []struct {
		name  string
		input *models.User
		err   bool
	}{
		{
			name: "test1",
			input: &models.User{
				Email:     "johndoe@gmail.com",
				Password:  "123456",
				FirstName: "John",
				LastName:  "Doe",
				Username:  "johndoe",
			},
			err: false,
		},
		{
			name: "duplicate username",
			input: &models.User{
				Email:     "johndoe2@gmail.com",
				Password:  "123456",
				FirstName: "John",
				LastName:  "Doe",
				Username:  "johndoe",
			},
			err: true,
		},
		{
			name: "dulicate email",
			input: &models.User{
				Email:     "johndoe@gmail.com",
				Password:  "123456",
				FirstName: "John",
				LastName:  "Doe",
				Username:  "johndoe2",
			},
			err: true,
		},
		{
			name: "no error",
			input: &models.User{
				Email:     "bobsmith@gmail.com",
				Password:  "1234",
				FirstName: "Bob",
				LastName:  "Smith",
				Username:  "bobsmth",
			},
			err: false,
		},
	}

	for _, u := range userTable {
		err := u.input.Create(context.TODO())

		if u.err && err == nil {
			t.Errorf("%v: expect %v; got %v", u.name, u.err, err)
		} else if !u.err && err != nil {
			t.Errorf("%v: expect %v; got %v", u.name, u.err, err)
		}
	}

	defer func() {
		for _, u := range userTable {
			_, err := models.UserCol().DeleteOne(context.TODO(), bson.M{"_id": u.input.ID})

			if err != nil {
				log.Fatal(err)
			}
		}
		log.Print("⚡️ Done ⚡️\n\n")
	}()

}

func TestCRUD(t *testing.T) {
	configs.LoadEnv()
	enc := json.NewEncoder(os.Stdout)

	user := models.User{
		Email:     faker.Email(),
		Password:  faker.Password(),
		FirstName: faker.FirstName(),
		LastName:  faker.LastName(),
		Username:  faker.Username(),
	}

	if err := user.Create(context.TODO()); err != nil {
		t.Errorf("expect ok; got %v", err)
	}
	rndInt, _ := faker.RandomInt(1, 1000)
	rndInt2, _ := faker.RandomInt(0, 1)
	var rndBool bool

	if rndInt2[0] == 0 {
		rndBool = false
	} else {
		rndBool = true
	}

	log.Println("-- Created User --")

	recipe := models.Recipe{
		UserID:   user.ID,
		Title:    faker.Name(),
		Amount:   &rndInt[0],
		Public:   &rndBool,
		Duration: &rndInt[2],
	}

	if err := recipe.Create(context.TODO()); err != nil {
		t.Errorf("expect ok; got %v", err)
	}

	log.Println("-- Created Recipe --")
	enc.Encode(recipe)
	log.Printf("\n\n")

	if err := recipe.Update(context.TODO(), &models.Recipe{
		Title: faker.Paragraph(),
	}); err != nil {
		t.Errorf("expect ok; got %v", err)
	}

	log.Println("-- Updated Recipe --")
	enc.Encode(recipe)
	log.Printf("\n\n")

	if err := recipe.Delete(context.TODO()); err != nil {
		t.Errorf("expect ok; got %v", err)
	}

	log.Println("-- Deleted Recipe --")

	defer func() {
		models.UserCol().DeleteOne(context.TODO(), bson.M{"_id": user.ID})
		log.Print("⚡️ Done ⚡️\n\n")
	}()
}
