dev:
	npx nodemon --exec go run src/main.go --signal SIGTERM

start:
	go run src/main.go

build:
	go build src/main.go

dcu:
	docker compose up -d

dcd:
	docker compose down

goseed:
	go run seed.go

gotest:
	go test -v ./test -run TestCRUD

heroku:
	heroku container:push web && heroku container:release web