package main

import (
	"fmt"
	"lynk/src/configs"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/recover"
)

func main() {

	configs.LoadEnv()

	app := fiber.New()
	app.Use(recover.New())
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
	}))

	port := os.Getenv("PORT")

	if port == "" {
		port = "5000"
	}

	configs.SetupRoutes(app)
	app.Get("/", func(c *fiber.Ctx) error {
		return c.JSON(fiber.Map{"message": "Hello World"})
	})

	app.Listen(fmt.Sprintf(":%v", port))
}
